package com.example.responsi5170411143

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.row.*

class Home : AppCompatActivity() {

    lateinit var listView : ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        listView = findViewById(R.id.ListView)
        var list = mutableListOf<Model>()

        list.add(Model("Tas Carrier Consina Alpine",   "Tas Carrier Consina dengan kapasitas 55L dijual dengan harga 465.000",   R.drawable.alpine  ))
        list.add(Model("Tas Carrier Consina Bering",   "Tas Carrier Consina dengan kapasitas 60L dijual dengan harga 595.000",   R.drawable.bering  ))
        list.add(Model("Tas Carrier Consina Centurion", "Tas Carrier Consina dengan kapasitas 50L dijual dengan harga 650.000", R.drawable.centurion  ))
        list.add(Model("Tas Daypack Consina Dash",  "Tas Consina Dash dengan kapasitas 10L dijual dengan harga 85.000",  R.drawable.dash  ))
        list.add(Model("Tas Carrier Consina Extraterestrial",  "Tas Carrier Consina dengan kapasitas 60L dijual dengan harga 595.000",  R.drawable.extrateres  ))
        list.add(Model("Tas Carrier Consina Gocta",  "Tas SemiCarrier Consina dengan kapasitas 30L",  R.drawable.gocta  ))
        list.add(Model("Tas Carrier Consina Horseshoe Bend",  "Tas Carrier Consina dengan kapasitas 80+10L dijual dengan harga 975.000",  R.drawable.horeshoebend  ))
        list.add(Model("Tas Carrier Consina Tarebbi",  "Tas Carrier Consina dengan kapasitas 60L dijual dengan harga 505.000",  R.drawable.tarebbi  ))
        list.add(Model("Tas Carrier Seven Summits Jataksara",  "Tas Carrier Seven Summits dengan kapasitas 60L dijual dengan harga 430.000",  R.drawable.jataksara  ))
        list.add(Model("Tas Carrier Kawanda",  "Tas Carrier Seven Summits dengan kapasitas 40L dijual dengan harga 330.000",  R.drawable.kawanda  ))

        listView.adapter = MyListAdapter(this,R.layout.row,list)

        listView.setOnItemClickListener{parent, view, position, id ->

            if (position==0){
                Toast.makeText(this, "Tas Carrier Consina dengan kapasitas 55L dijual dengan harga 465.000",   Toast.LENGTH_SHORT).show()
            }
            if (position==1){
                Toast.makeText(this, "Tas Carrier Consina dengan kapasitas 60L dijual dengan harga 595.000",   Toast.LENGTH_SHORT).show()
            }
            if (position==2){
                Toast.makeText(this, "Tas Carrier Consina dengan kapasitas 50L dijual dengan harga 650.000", Toast.LENGTH_SHORT).show()
            }
            if (position==3){
                Toast.makeText(this, "Tas Consina Dash dengan kapasitas 10L dijual dengan harga 85.000",  Toast.LENGTH_SHORT).show()
            }
            if (position==4){
                Toast.makeText(this, "Tas Carrier Consina dengan kapasitas 60L dijual dengan harga 595.000",  Toast.LENGTH_SHORT).show()
            }
            if (position==5){
                Toast.makeText(this, "Tas SemiCarrier Consina dengan kapasitas 30L",  Toast.LENGTH_SHORT).show()
            }
            if (position==6){
                Toast.makeText(this, "Tas Carrier Consina dengan kapasitas 80+10L dijual dengan harga 975.00",  Toast.LENGTH_SHORT).show()
            }
            if (position==7){
                Toast.makeText(this, "Tas Carrier Consina dengan kapasitas 60L dijual dengan harga 505.000",  Toast.LENGTH_SHORT).show()
            }
            if (position==8){
                Toast.makeText(this, "Tas Carrier Seven Summits dengan kapasitas 60L dijual dengan harga 430.000",  Toast.LENGTH_SHORT).show()
            }
            if (position==9){
                Toast.makeText(this, "Tas Carrier Seven Summits dengan kapasitas 40L dijual dengan harga 330.000",  Toast.LENGTH_SHORT).show()
            }
        }

    }

}
