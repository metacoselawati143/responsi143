package com.example.responsi5170411143

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val reg = findViewById(R.id.Daftar) as TextView

        reg.setOnClickListener{
            intent = Intent(this, Register::class.java)
            startActivity(intent)
        }


        login.setOnClickListener {
            val user = user.text.toString()
            val pass = pass.text.toString()
            if (user == "metacoselawati" || pass == "meta143") {
                intent = Intent(this, Home::class.java)
                startActivity(intent)
            }else{
                Toast.makeText(this, "Username atau Password Salah", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
